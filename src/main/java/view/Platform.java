package view;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.*;
import model.Character;


import java.util.ArrayList;

public class Platform extends Pane {

    public static final int WIDTH = 1200;
    public static final int HEIGHT = 600;
    public static final int GROUND = 555;

    private Image platformImg;
    private ArrayList<Character> characterList = new ArrayList();
    private ArrayList<Score> scoreList = new ArrayList<>();
    private Ball ball;
    private Keys keys;
    private Wall wall;
    private Label time;

    public Platform() {
        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/assets/background1.png"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        characterList.add(new Character(30, GROUND- Character.HEIGHT,0,0, KeyCode.A,KeyCode.D,KeyCode.W,KeyCode.SPACE ,KeyCode.Q, KeyCode.E, KeyCode.R, KeyCode.F,1));
        characterList.add(new Character(Platform.WIDTH-30, GROUND - Character.HEIGHT,0,0, KeyCode.LEFT,KeyCode.RIGHT,KeyCode.UP,KeyCode.K, KeyCode.J, KeyCode.L, KeyCode.N, KeyCode.M, 2));
        scoreList.add(new Score(30,30));
        scoreList.add(new Score(WIDTH-90,30));
        ball = new Ball(this,180,100,17f,50f);
        wall = new Wall();
        getChildren().add(backgroundImg);
        getChildren().addAll(scoreList);
        getChildren().addAll(characterList);
        getChildren().add(ball);
        getChildren().add(wall);

        time = new Label("60");
        time.setFont(Font.font("Verdana", FontWeight.BOLD,40));
        time.setTextFill(Color.web("#FFF"));
        time.setTranslateX(WIDTH/2);
        getChildren().add(time);


    }

    public ArrayList<Character> getCharacterList() {
        return characterList;
    }

    public ArrayList<Score> getScoreList() {
        return scoreList;
    }

    public Wall getWall() {
        return wall;
    }

    public Ball getBall() { return ball; }
    public Keys getKeys() { return keys;
    }
    public void setTimer(int t){ time.setText(String.valueOf(t)); }
}

